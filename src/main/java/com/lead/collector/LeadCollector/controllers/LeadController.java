package com.lead.collector.LeadCollector.controllers;

import com.lead.collector.LeadCollector.enums.TipoDeLead;
import com.lead.collector.LeadCollector.models.Lead;
import com.lead.collector.LeadCollector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/lead")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping
    public Iterable<Lead> buscarTodosLeads(){
        return leadService.buscaTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);

        if (leadOptional.isPresent()){
            return  leadOptional.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Lead> salvarLead(@RequestBody Lead lead){
        Lead leadObjeto = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(leadObjeto);
    }

    @PutMapping("/{id}")
    public Lead atualizaLead(@PathVariable Integer id, @RequestBody Lead lead){
        lead.setId(id);
        Lead leadObjeto = leadService.atualizarLead(lead);
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public Lead deletarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()){
            leadService.deletarLead(leadOptional.get());
            return leadOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

}

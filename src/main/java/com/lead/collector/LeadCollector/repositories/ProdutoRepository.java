package com.lead.collector.LeadCollector.repositories;

import com.lead.collector.LeadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}

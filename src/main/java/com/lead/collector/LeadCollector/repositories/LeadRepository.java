package com.lead.collector.LeadCollector.repositories;

import com.lead.collector.LeadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
}

package com.lead.collector.LeadCollector.enums;

public enum TipoDeLead {
    QUENTE,
    ORGANICO,
    FRIO
}

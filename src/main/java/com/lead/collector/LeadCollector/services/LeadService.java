package com.lead.collector.LeadCollector.services;

import com.lead.collector.LeadCollector.enums.TipoDeLead;
import com.lead.collector.LeadCollector.models.Lead;
import com.lead.collector.LeadCollector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {


    @Autowired
    private LeadRepository leadRepository;

    public Optional<Lead> buscarPorId(int id) {
        Optional<Lead> leadOptional = leadRepository.findById(id);
        return leadOptional;
    }

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscaTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead atualizarLead(Lead lead) {
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if (leadOptional.isPresent()) {
            Lead leadData = leadOptional.get();
            if (lead.getNome() == null) {
                lead.setNome((leadData.getNome()));
            }
            if (lead.getEmail() == null) {
                lead.setEmail(leadData.getEmail());
            }
            if (lead.getTipoDeLead() == null) {
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }


}
